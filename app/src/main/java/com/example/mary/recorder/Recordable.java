package com.example.mary.recorder;

/**
 * Created by mary on 07.09.15.
 */
public interface Recordable {

    public abstract void record();  //record method
    public abstract void play(); //play method
    public abstract void stop(); //stop method
}
