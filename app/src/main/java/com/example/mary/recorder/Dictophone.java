package com.example.mary.recorder;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.io.IOException;


public class Dictophone extends Activity implements Recordable {

    Button play, stop, record;
    private MediaRecorder recorder;
    private String outputFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictophone);
        play = (Button) findViewById(R.id.play);
        stop = (Button) findViewById(R.id.stop);
        record = (Button) findViewById(R.id.record);
        setClicklistener();//setting OnClicklistener for buttons
        setAudioRecorder(); //setting  media recorder


    }

    public void setClicklistener(){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.record:record();break;
                    case R.id.play:play();break;
                    case R.id.stop : stop();break;
                }
            }
        };
        record.setOnClickListener(listener);
        play.setOnClickListener(listener);
        stop.setOnClickListener(listener);
        stop.setEnabled(false);
        play.setEnabled(false);
    }

    public void setAudioRecorder(){
        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp";
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        recorder.setOutputFile(outputFile);
    }
    @Override
    public void record() {
        try {
            recorder.prepare();
            recorder.start();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        record.setEnabled(false);
        stop.setEnabled(true);
        Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
    }


    @Override
    public void play() throws IllegalArgumentException, SecurityException, IllegalStateException {
        MediaPlayer m = new MediaPlayer();
        try {
            m.setDataSource(outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            m.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        m.start();
        Toast.makeText(getApplicationContext(), "Playing audio", Toast.LENGTH_LONG).show();
     }


    @Override
    public void onBackPressed() {//if back pressed, stop recording
        // TODO Auto-generated method stub
        super.onBackPressed();
        recorder.stop();
        recorder.release();
        recorder = null;
        Toast.makeText(getApplicationContext(), "Audio recorded successfully",Toast.LENGTH_LONG).show();
    }
    @Override
    public void stop() {
        recorder.stop();
        recorder.release();
        recorder = null;
        stop.setEnabled(false);
        play.setEnabled(true);
        Toast.makeText(getApplicationContext(), "Audio recorded successfully",Toast.LENGTH_LONG).show();
    }

}